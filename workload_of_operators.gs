//**Create custom menu item
function onOpen() {
  var customMenuItem = SpreadsheetApp.getUi();
  customMenuItem.createMenu('Селектор')
  .addItem('▶ Создать новый отчёт', 'makeReport')
  .addSeparator()
  .addItem('↻ Обновить текущий отчёт', 'refreshReport')
  .addToUi();
}
//** Main function
function makeReport() {
  //sheet for data manipulation
  var spreadsheetId = SpreadsheetApp.getActiveSpreadsheet().getId();
  var sheetData = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('DATA');
  var sheetMoreThenFourteen = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('В работе более 14ти дней');
  var sheetId = sheetData.getSheetId();
  var ssReport = SpreadsheetApp; 
  var ss = SpreadsheetApp.openById('Spreadsheet Id'); //Spreadsheet with data
  var sheet = ss.getSheetByName('Sheet Name');
  var range = sheet.getRange(1, 9, sheet.getLastRow(), 1);
  var values = range.getValues();
  var emptyCells = [];
  var sheetName = 'Загрузка всех операторов по логинам ' + new Date().getDate() + '.' + ('0' + (new Date().getUTCMonth() + 1)).slice(-2) + '.' + new Date().getUTCFullYear();
  //check the available report for the current date
  if(ssReport.getActiveSheet().getName() == sheetName){
    ssReport.getUi().alert('Отчёт за текущую дату уже сформирован. Вы можете обновить его в меню "Селектор"');
    return;
  }
  //Valid empty cells in column "column name"; if this column has at least one empty cell function alert message and stops(!!!)
  for(var i = 0; i < values.length; i++){
    if(values[i][0] == ''){
      emptyCells.push(i + 1);
    }
  }
  if(emptyCells.length > 0){
    ssReport.getUi().alert('Отсутствует название продукта в строке: ' + emptyCells);
    return;
  }
  var newSheet = duplicateOldSheet();
  copyData(sheetData, sheet);
  pmProdPivitTable(sheetData, sheetId, spreadsheetId);
  pmStatusPivitTable(sheetData, sheetId, spreadsheetId);
  expProdPivitTable(sheetData, sheetId, spreadsheetId);
  expStatusPivitTable(sheetData, sheetId, spreadsheetId);
  copyFromPivot(sheetData, newSheet);
  copyFromDataToMoreThenFourteen(sheetData, sheetMoreThenFourteen, spreadsheetId);
}
//** to refresh current report & check if report for current date is presents
function refreshReport() {
  var spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  var currentDate = new Date();
  var currentMonth = ('0' + (currentDate.getUTCMonth() + 1)).slice(-2);
  var currentYear = currentDate.getUTCFullYear();
  var sheetName = 'workload name ' + currentDate.getDate() + '.' + currentMonth + '.' + currentYear;
  var oldSheetToday = sheetName + '(old)';
  if(spreadSheet.getSheetByName(oldSheetToday)){
    var oldSheet = spreadSheet.getSheetByName(oldSheetToday);
    spreadSheet.deleteSheet(oldSheet);
  };
  if(sheetName == spreadSheet.getActiveSheet().getName()){
    spreadSheet.getActiveSheet().setName(spreadSheet.getActiveSheet().getName() + '(old)');
    makeReport();
  }else{
    var message = SpreadsheetApp.getUi().alert('Отчёт за текущую дату отсутствует! Вы можете создать его в меню "Селектор"');
    return;
  }
}
//** Create a duplicate active sheet with pivot information 
function duplicateOldSheet() {
  var currentDate = new Date();
  var currentMonth = ('0' + (currentDate.getUTCMonth() + 1)).slice(-2);
  var currentYear = currentDate.getUTCFullYear();
  //create duplicate
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  var newSheet = SpreadsheetApp.getActiveSpreadsheet().duplicateActiveSheet().activate();
  //rename new sheet depending on current date
  var newName = 'workload name ' + currentDate.getDate() + '.' + currentMonth + '.' + currentYear;
  newSheet.setName(newName);
  newSheet.getRange("C5:R13").clearContent();
  newSheet.getRange("C19:Q27").clearContent();
  newSheet.getRange("C34:R57").clearContent();
  newSheet.getRange("C63:Q86").clearContent();
  //to hide sheet with previously information
  sheet.hideSheet();
  return newSheet;
}
//** Copy all necessary data from main spreadsheet 'Main Spreadsheet name' 
function copyData(sheetData, sheet) {
  sheetData.getDataRange().clearContent();
  var status = ['first_row_name',
                'status_name_1',
                'status_name_2',
                'status_name_3',
                'status_name_4',
                'status_name_5',
                'status_name_6',
                'status_name_7',
                'status_name_8',
                'status_name_9',
                'status_name_10',
                'status_name_11',
                'status_name_12',
                'status_name_13',
                'status_name_14',
                '']; //с явным указанием пустых ячеек для сравнения, функция работает быстрее и точнее
  var values = sheet.getRange('A:AC').getValues(); //assign the range you want to copy
  var data = [];    
  for(var i = 0; i < values.length; i++){
    for(var j = 0; j < status.length; j++){
      if(values[i][20] == status[j]) {
        data.push(values[i]);
      }
    }
  }sheetData.getRange(1, 1, data.length, data[0].length).setValues(data); //you will need to define the size of the copied data see getRange()
  //(!)delete unnecessary columns  
  sheetData.deleteColumns(4, 5);
  sheetData.deleteColumns(5, 3);
  sheetData.deleteColumns(6, 2);
  sheetData.deleteColumns(7, 4);
  sheetData.deleteColumns(9, 1);
  sheetData.deleteColumns(10, 1);
  sheetData.deleteColumns(12, 1);
  sheetData.insertColumnAfter(4);
  sheetData.insertColumnAfter(8);
  var prodToCopy = sheetData.getRange(1, 4, sheetData.getLastRow(), 1);
  prodToCopy.copyTo(sheetData.getRange(1, 5));
  var statusToCopy = sheetData.getRange(1, 8, sheetData.getLastRow(), 1);
  statusToCopy.copyTo(sheetData.getRange(1, 9));
  sheetData.insertColumnsAfter(sheetData.getLastColumn(), 40); //add empty colunms for pivot tables
  validPeople(sheetData);
}
//** Validation of managers and experts according to their levels
function validPeople(sheetData) {
  var managersList = ['manager_1',
                      'manager_2',
                      'manager_3',
                      'manager_4',
                      'manager_5',
                      'manager_6',
                      'manager_7',
                      'manager_8'];
  
  var expertsList = ['expert_1',
                     'expert_2',
                     'expert_3',
                     'expert_4',
                     'expert_5',
                     'expert_6',
                     'expert_7',
                     'expert_8',
                     'expert_9',
                     'expert_10',
                     'expert_11',
                     'expert_12',
                     'expert_13',
                     'expert_14',
                     'expert_15',
                     'expert_16',
                     'expert_17',
                     'expert_18',
                     'expert_19',
                     'expert_20',
                     'expert_21',
                     'expert_22',
                     'expert_23'];

  var range1 = sheetData.getRange(1, 11, sheetData.getLastRow(), 1);
  var values1 = range1.getValues();
  var range2 = sheetData.getRange(1, 10, sheetData.getLastRow(), 1);
  var values2 = range2.getValues();
    
  for(var i = 0; i < values1.length; i++){
    for(var j = 0; j < managersList.length; j++){
      if(values1[i][0] == managersList[j] || values1[i][0] == '') {
        var cell = range1.getCell(i + 1, 1);
        cell.setValue('O1 NOT FOUND'); //необходимо для того, чтобы в сводной такие значения (== 'Не указан О1') были последними для алфавитной сортировки 
      }
    }
  }
  for(var i = 0; i < values2.length; i++){
    for(var j = 0; j < expertsList.length; j++){
      if(values2[i][0] == expertsList[j] || values2[i][0] == '') {
        var cell = range2.getCell(i + 1, 1);
        cell.setValue('O2 NOT FOUND'); //необходимо для того, чтобы в сводной такие значения (== 'Не указан О2') были последними для алфавитной сортировки 
      }
    }
  }
  validGeneralProducts(sheetData);
}
//** Change product's names for pivot tables on active sheet
function validGeneralProducts(sheetData) {
  var range = sheetData.getRange(2, 4, sheetData.getLastRow() - 1, 1);
  var values = range.getValues();  
  
  for(var i = 0; i < values.length; i++){
    if(values[i][0] == 'some product (new)'){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('some product');
    }else if(values[i][0] == 'change conditions_1'){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('change');
    }else if(values[i][0] == 'change conditions_2'){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('change');
    }else if(values[i][0] == 'change conditions_3'){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('change');
    }
  }
  validOtherProducts(sheetData);
}
//** Change other product's names for pivot tables on active sheet
function validOtherProducts(sheetData) {
  var prodForChange = ['Product_1',
                       'Product_2',
                       'Product_3',
                       'Product_4',
                       'Product_5',
                       'Product_6',
                       'Product_7',
                       'Product_8',
                       'Product_9',
                       'Product_10',
                       'Product_11',
                       'Product_12',
                       'Product_13',
                       'Product_14',
                       'Product_15'];

  var range = sheetData.getRange(2, 4, sheetData.getLastRow() - 1, 1);
  var values = range.getValues();
  var count = 0;
  for(var i = 0; i < values.length; i++){
    for(var j = 0; j < prodForChange.length; j++){
      if(values[i][0] == prodForChange[j]){
        count++;
      }
    }if(count == 0){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('other product');
    }count = 0;
  }
  validStatus(sheetData);
}
//** Change product's statuses for pivot tables on active sheet
function validStatus(sheetData) {
  var range = sheetData.getRange(2, 8, sheetData.getLastRow() - 1, 1);
  var values = range.getValues();
  for(var i = 0; i < values.length; i++){
    if(values[i][0] == 'Remarks of head office ......'){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('Remarks of head office');
    }else if(values[i][0] == ''){
      var cell = range.getCell(i + 1, 1);
      cell.setValue('Customer contact');
    }
  }
  //add special row for FULL pivot table like in Spreadsheet Workload - sheet Workload all...
  var addRow = [['', '', '', 'NOT FOUND', '', '', '', 'NOT FOUND', '', 'O2 NOT FOUND', 'O1 NOT FOUND', '', '', '']];
  sheetData.getRange(sheetData.getLastRow() + 1, 1, addRow.length, addRow[0].length).setValues(addRow);
}
//** Create PIVOT table product's names - managers
function pmProdPivitTable(sheetData, sheetId, spreadsheetId) {
  var range = sheetData.getRange('A:J');
  var val = range.getValues();
  var start = [sheetId, 0, 15];
  var values = ['name', 0, 'COUNTA']
  var source = [sheetId, 0, 0, sheetData.getLastRow(), val[0].length];
  var rows = [[9, true, "ASCENDING", 9]];
  var columns = [[3, true, "ASCENDING", 3]];
  createPivotTable(source, rows, columns, values, start, spreadsheetId);
}
//** Create PIVOT table product's statuses - managers
function pmStatusPivitTable(sheetData, sheetId, spreadsheetId) {
  var range = sheetData.getRange('A:J');
  var val = range.getValues();
  var start = [sheetId, 14, 15];
  var values = ['name', 0, 'COUNTA']
  var source = [sheetId, 0, 0, sheetData.getLastRow(), val[0].length];
  var rows = [[9, true, "ASCENDING", 9]];
  var columns = [[7, true, "ASCENDING", 7]];
  createPivotTable(source, rows, columns, values, start, spreadsheetId);
}
//** Create PIVOT table product's names - experts
function expProdPivitTable(sheetData, sheetId, spreadsheetId) {
  var range = sheetData.getRange('A:K');
  var val = range.getValues();
  var start = [sheetId, 28, 15];
  var values = ['name', 0, 'COUNTA']
  var source = [sheetId, 0, 0, sheetData.getLastRow(), val[0].length];
  var rows = [[10, true, "ASCENDING", 10]];
  var columns = [[3, true, "ASCENDING", 3]];
  createPivotTable(source, rows, columns, values, start, spreadsheetId);
}
//**Create PIVOT table product's statuses - experts
function expStatusPivitTable(sheetData, sheetId, spreadsheetId) {
  var range = sheetData.getRange('A:K');
  var val = range.getValues();
  var start = [sheetId, 57, 15];
  var values = ['name', 0, 'COUNTA']
  var source = [sheetId, 0, 0, sheetData.getLastRow(), val[0].length];
  var rows = [[10, true, "ASCENDING", 10]];
  var columns = [[7, true, "ASCENDING", 7]];
  createPivotTable(source, rows, columns, values, start, spreadsheetId);
}
//** main function for creating PIVOT tables
function createPivotTable(source, rows, columns, values, start, spreadsheetId) {
  var rowsData = [];
  var columnsData = [];
  for(var r in rows){
    var element = {
                    "sourceColumnOffset": rows[r][0],
                    "showTotals": rows[r][1],
                    "sortOrder": rows[r][2],
                    "valueBucket": {
                      "valuesIndex": rows[r][3],
                    }
                  }
    rowsData.push(element);
  }
  for(var c in columns){
    var element = {
                    "sourceColumnOffset": columns[c][0],
                    "showTotals": columns[c][1],
                    "sortOrder": columns[c][2],
                    "valueBucket": {
                      "valuesIndex": columns[c][3],
                    }
                  }
    columnsData.push(element);
  }
  var resource = {
    "requests": [
      {
        "updateCells": {
          "rows": [
            {
              "values": [
                {
                  "pivotTable": {
                    "rows": rowsData,
                    "columns": columnsData,
                    "source": {
                      "sheetId": source[0],
                      "startRowIndex": source[1],
                      "startColumnIndex": source[2],
                      "endRowIndex": source[3],
                      "endColumnIndex": source[4]
                    },
                    "values": [
                      {
                        "name": values[0],
                        "sourceColumnOffset": values[1],
                        "summarizeFunction": values[2]
                      }
                    ],
                    "criteria": {},
                    "valueLayout": "HORIZONTAL"
                  }
                }
              ]
            }
          ],
          "start": {
            "sheetId": start[0],
            "rowIndex": start[1],
            "columnIndex": start[2]
          },
          "fields": "PivotTable"
        }
      }
    ]
  }
  Sheets.Spreadsheets.batchUpdate(resource, spreadsheetId);
}
//** Copy data to report 
function copyFromPivot(sheetData, newSheet) {
  var range = sheetData.getRange(1, 16, sheetData.getLastRow(), sheetData.getLastColumn());
  range.copyValuesToRange(sheetData, 16, sheetData.getLastColumn(), 1, sheetData.getLastRow());
  //select & copy data for products O2
  var valuesCheckData = sheetData.getRange(2, 17, 1, sheetData.getLastColumn()).getValues()[0];
  var valuesCheckLoad = newSheet.getRange(4, 3, 1, 16).getValues()[0];
  for(var i = 0; i < valuesCheckData.length; i++){
    for(var j = 0; j < valuesCheckLoad.length; j++){
      if(valuesCheckData[i] == valuesCheckLoad[j]){
        var data = sheetData.getRange(3, i + 17, 9, 1).getValues();
        newSheet.getRange(5, j + 3, 9, 1).setValues(data);
      }
    }
  }
  //select & copy data for stage O2
  var valuesCheckData = sheetData.getRange(16, 17, 1, sheetData.getLastColumn()).getValues();
  var valuesCheckLoad = newSheet.getRange(18, 3, 1, 15).getValues();
  for(var i = 0; i < valuesCheckData[0].length; i++){
    for(var j = 0; j < valuesCheckLoad[0].length; j++){
      if(valuesCheckData[0][i] == valuesCheckLoad[0][j]){
        var data = sheetData.getRange(17, i + 17, 9, 1).getValues();
        newSheet.getRange(19, j + 3, 9, 1).setValues(data);
      }
    }
  }
  //select & copy data for products O1
  var valuesCheckData = sheetData.getRange(30, 17, 1, sheetData.getLastColumn()).getValues();
  var valuesCheckLoad = newSheet.getRange(33, 3, 1, 16).getValues();
  for(var i = 0; i < valuesCheckData[0].length; i++){
    for(var j = 0; j < valuesCheckLoad[0].length; j++){
      if(valuesCheckData[0][i] == valuesCheckLoad[0][j]){
        var data = sheetData.getRange(31, i + 17, 24, 1).getValues();
        newSheet.getRange(34, j + 3, 24, 1).setValues(data);
      }
    }
  }
  //select & copy data for stage O1
  var valuesCheckData = sheetData.getRange(59, 17, 1, sheetData.getLastColumn()).getValues();
  var valuesCheckLoad = newSheet.getRange(62, 3, 1, 15).getValues();
  for(var i = 0; i < valuesCheckData[0].length; i++){
    for(var j = 0; j < valuesCheckLoad[0].length; j++){
      if(valuesCheckData[0][i] == valuesCheckLoad[0][j]){
        var data = sheetData.getRange(60, i + 17, 24, 1).getValues();
        newSheet.getRange(63, j + 3, 24, 1).setValues(data);
      }
    }
  }
}
//** Chamge data & copy it to sheet More Then Fourteen Days
function copyFromDataToMoreThenFourteen(sheetData, sheetMoreThenFourteen, spreadsheetId) {
  sheetData.deleteColumns(4, 1);
  sheetData.deleteColumns(7, 1);
  sheetData.deleteColumns(9, 1);
  var sheetId = sheetMoreThenFourteen.getSheetId();
  var data = [];
  //Get date which is 14 days less then current (without time(!) )
  var today = new Date;
  var fourteenDays = 14 * 24 * 60 * 60 * 1000; // Количество миллисекунд в 14 днях
  var deadline = new Date(today.getTime() - fourteenDays);
  deadline.setHours(0, 0, 0, 0);
  var ms = Number(deadline.getTime()).toFixed(0);

  var range = sheetData.getRange(2, 1, sheetData.getLastRow(), 11);
  var values = range.getValues();
  for(var i = 0; i < values.length; i++){
    var cellValue = new Date(values[i][0]);
    var msData = Number(cellValue.getTime()).toFixed(0);
    if(msData < ms){
      data.push(values[i]);
    }
  }
  var requests = [{
        "clearBasicFilter": {
        "sheetId": sheetId
        }
    }];
  Sheets.Spreadsheets.batchUpdate({'requests': requests}, spreadsheetId);
  var deleteData = sheetMoreThenFourteen.getRange(2, 1, sheetMoreThenFourteen.getLastRow(), sheetMoreThenFourteen.getLastColumn()).clearContent();
  sheetMoreThenFourteen.getRange(2, 1, data.length, data[0].length).setValues(data).sort({column: 6, ascending: false});
  hideDataSheet(sheetData);
}
//** to hide work sheet DATA
function hideDataSheet(sheetData) {
  sheetData.hideSheet();
}